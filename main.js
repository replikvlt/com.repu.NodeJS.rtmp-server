const RtmpServer = require('rtmp-server');
const rtmpServer = new RtmpServer();


/*Openshift inbuild constants*/
var server_port = process.env.OPENSHIFT_NODEJS_PORT  || 1935;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

console.log("Server started!\
On adress: "+server_ip_address+":"+server_port); 

rtmpServer.on('error', err => {
  throw err;
});


rtmpServer.on('client', client => {
  //client.on('command', command => { 
  //  console.log(command.cmd, command); 
  //}); 

  client.on('connect', () => {
     console.log('connect', client.app);
  });
  
  client.on('play', ({ streamName }) => {
    console.log('PLAY', streamName);
  });
  
  client.on('publish', ({ streamName }) => {
    console.log('PUBLISH', streamName);
  });
  
  client.on('stop', () => {
    console.log('client disconnected');
  });
});
 
rtmpServer.listen(server_port, server_ip_address) ; /*port was 1935 before changed to 8080*/
/*changed ports to openshift constants*/
